import 'package:get_retrofit_boilerplate/singletons/singletons_index.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextFieldWidget extends StatelessWidget {
  const CustomTextFieldWidget({
    Key? key,
    this.controller,
    this.inputFormatters,
    this.textInputAction,
    this.initialValue,
    this.obscureText,
    this.maxLines,
    this.minLine,
    this.maxLength,
    this.keyboardType,
    this.textAlign,
    this.onChanged,
    this.validator,
    this.enable,
    this.style,

    // Inside decoration
    this.hintText,
    this.hintStyle,
    this.filled,
    this.fillColor,
    this.prefixIcon,
    this.suffixIcon,
    this.contentPadding,
    this.errorText,

    // Border
    this.border,
    this.enabledBorder,
    this.focusedBorder,
    this.disabledBorder,
    this.errorBorder,
    this.focusedErrorBorder,
    this.isStakeOnTextField,
  }) : super(key: key);

  final TextEditingController? controller;
  final List<TextInputFormatter>? inputFormatters;
  final TextInputAction? textInputAction;
  final String? initialValue;
  final bool? obscureText;
  final int? maxLines;
  final int? minLine;
  final int? maxLength;
  final TextInputType? keyboardType;
  final TextAlign? textAlign;
  final Function(String)? onChanged;
  final String? Function(String?)? validator;
  final bool? enable;
  final TextStyle? style;

  // Inside decoration
  final String? hintText;
  final TextStyle? hintStyle;
  final bool? filled;
  final Color? fillColor;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final EdgeInsetsGeometry? contentPadding;
  final String? errorText;

  // Borders
  final InputBorder? border;
  final InputBorder? enabledBorder;
  final InputBorder? focusedBorder;
  final InputBorder? disabledBorder;
  final InputBorder? errorBorder;
  final InputBorder? focusedErrorBorder;

  final bool? isStakeOnTextField;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        TextFormField(
          controller: controller,
          textInputAction: textInputAction,
          initialValue: initialValue,
          obscureText: obscureText ?? false,
          inputFormatters: inputFormatters,
          maxLines: maxLines,
          minLines: minLine,
          maxLength: maxLength,
          keyboardType: keyboardType,
          textAlign: textAlign ?? TextAlign.start,
          onChanged: onChanged,
          validator: validator,
          enabled: enable,
          style: style ?? TextStyle(fontSize: Metrics.instance.bodyMedium),
          decoration: InputDecoration(
            hintText: hintText,
            hintStyle: hintStyle,
            filled: filled,
            fillColor: fillColor,
            suffixIcon: suffixIcon,
            prefixIcon: prefixIcon,
            contentPadding: contentPadding,
            errorText: errorText,
            border: border,
            enabledBorder: enabledBorder,
            focusedBorder: focusedBorder,
            disabledBorder: disabledBorder,
            errorBorder: errorBorder,
            focusedErrorBorder: focusedErrorBorder,
          ),
        ),
        (isStakeOnTextField ?? false)
            ? Positioned(
                child: Container(
                  height: 55,
                  width: double.infinity,
                  color: Colors.transparent,
                ),
              )
            : const SizedBox.shrink(),
      ],
    );
  }
}
