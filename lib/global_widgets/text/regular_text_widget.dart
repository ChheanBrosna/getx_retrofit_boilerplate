import 'package:get_retrofit_boilerplate/singletons/singletons_index.dart';
import 'package:flutter/material.dart';

class RegularTextWidget extends StatelessWidget {
  const RegularTextWidget({
    Key? key,
    required this.text,
    this.fontSize,
    this.fontWeight,
    this.textAlign,
    this.color,
    this.decoration,
    this.maxLines,
    this.height,
    this.letterSpacing,
    this.overflow,
  }) : super(key: key);

  final String text;
  final TextOverflow? overflow;
  final double? fontSize;
  final FontWeight? fontWeight;
  final Color? color;
  final TextDecoration? decoration;
  final double? height;
  final double? letterSpacing;
  final TextAlign? textAlign;
  final int? maxLines;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      overflow: overflow ?? TextOverflow.ellipsis,
      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
            letterSpacing: letterSpacing,
            height: height,
            decoration: decoration,
            color: color,
            fontWeight: fontWeight,
            fontSize: fontSize ?? Metrics.instance.bodyMedium,
          ),
      textAlign: textAlign,
      maxLines: maxLines,
    );
  }
}
