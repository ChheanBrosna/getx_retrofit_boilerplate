import 'package:get_retrofit_boilerplate/singletons/singletons_index.dart';
import 'package:flutter/material.dart';

class ErrorDialogWidget extends StatelessWidget {
  const ErrorDialogWidget({
    Key? key,
    this.title,
    this.content,
    this.titleColor,
    this.contentColor,
    this.buttonBgColor,
    this.onPressed,
  }) : super(key: key);

  final String? title;
  final String? content;
  final Color? titleColor;
  final Color? contentColor;
  final Color? buttonBgColor;
  final Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    void handleClosePressed() {
      Navigator.of(context).pop();
      if (onPressed != null) {
        onPressed!();
      }
    }

    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Metrics.instance.defaultRadius),
      ),
      insetPadding: EdgeInsets.all(Metrics.instance.defaultPadding),
      clipBehavior: Clip.hardEdge,
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.all(Metrics.instance.defaultPadding),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            // Title
            Text(
              title ?? 'ERROR',
              style: Theme.of(context).textTheme.bodyLarge!.copyWith(color: titleColor),
            ),

            SizedBox(height: Metrics.instance.medium),

            // Content
            Text(
              content ?? 'N/A',
              maxLines: 2,
              style: Theme.of(context).textTheme.bodyMedium!.copyWith(color: contentColor),
            ),

            SizedBox(height: Metrics.instance.extraHuge),

            // Close
            ElevatedButton(
              onPressed: handleClosePressed,
              child: const Text('CLOSE'),
            )
          ],
        ),
      ),
    );
  }
}
