import 'package:get_retrofit_boilerplate/constants/constants_index.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'product_repository.g.dart';

@RestApi(baseUrl: UrlConstant.baseUrl)
abstract class ProductRepository {
  factory ProductRepository(Dio dio, {String baseUrl}) = _ProductRepository;

  @GET(EndPointConstant.getProduct)
  Future<dynamic> getProduct();
}
