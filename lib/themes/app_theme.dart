import 'package:get_retrofit_boilerplate/singletons/singletons_index.dart';
import 'package:get_retrofit_boilerplate/themes/themes_index.dart';
import 'package:flutter/material.dart';

class AppTheme {
  const AppTheme._();

  static ThemeData define() {
    return ThemeData(
      primaryColor: AppColors.primary,
      primarySwatch: MaterialColorGenerator.generateMaterialColor(AppColors.primary),

      scaffoldBackgroundColor: AppColors.bgColor,

      /// AppBar theme pre-define
      appBarTheme: AppBarTheme(
        backgroundColor: AppColors.white,
        titleTextStyle: TextStyle(
          color: AppColors.black,
          fontWeight: FontWeight.bold,
          fontSize: Metrics.instance.appBarSize,
        ),
        iconTheme: const IconThemeData(color: Colors.black),
        centerTitle: true,
        elevation: .8,
      ),

      /// TextField theme pre-define
      inputDecorationTheme: InputDecorationTheme(
        fillColor: AppColors.textFieldBgColor,
        filled: true,
        hintStyle: const TextStyle(color: Colors.grey),
        contentPadding: EdgeInsets.symmetric(
          vertical: Metrics.instance.textFieldVerticalPadding,
          horizontal: Metrics.instance.textFieldHorizontalPadding,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(Metrics.instance.textFieldBorderRadius),
          borderSide: const BorderSide(color: AppColors.textFieldBorderColor),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(Metrics.instance.textFieldBorderRadius),
          borderSide: const BorderSide(color: AppColors.textFieldBorderColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(Metrics.instance.textFieldBorderRadius),
          borderSide: const BorderSide(color: AppColors.primary),
        ),
        disabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(Metrics.instance.textFieldBorderRadius),
          borderSide: const BorderSide(color: AppColors.textFieldBorderColor),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(Metrics.instance.textFieldBorderRadius),
          borderSide: const BorderSide(color: AppColors.textFieldBorderColor),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(Metrics.instance.textFieldBorderRadius),
          borderSide: const BorderSide(color: Colors.red),
        ),
      ),

      /// Text theme pre-define
      textTheme: TextTheme(
        /// Extremely large text.
        displayLarge: TextStyle(
          color: AppColors.primaryTextColor,
          fontSize: Metrics.instance.displayLarge,
          fontWeight: FontWeight.w300,
          letterSpacing: -1.5,
          overflow: TextOverflow.ellipsis,
        ),

        /// Very, very large text.
        displayMedium: TextStyle(
          color: AppColors.primaryTextColor,
          fontSize: Metrics.instance.displayMedium,
          fontWeight: FontWeight.w300,
          letterSpacing: -0.5,
          overflow: TextOverflow.ellipsis,
        ),

        /// Very large text.
        displaySmall: TextStyle(
          color: AppColors.primaryTextColor,
          fontSize: Metrics.instance.displaySmall,
          fontWeight: FontWeight.w400,
          overflow: TextOverflow.ellipsis,
        ),

        /// Large text.
        headlineLarge: TextStyle(
          color: AppColors.primaryTextColor,
          fontSize: Metrics.instance.headlineLarge,
          fontWeight: FontWeight.w600,
          letterSpacing: 0.25,
          overflow: TextOverflow.ellipsis,
        ),

        /// Used for large text in dialogs
        headlineMedium: TextStyle(
          color: AppColors.primaryTextColor,
          fontSize: Metrics.instance.headlineMedium,
          fontWeight: FontWeight.w600,
          overflow: TextOverflow.ellipsis,
        ),

        /// Used for large text in dialogs
        headlineSmall: TextStyle(
          color: AppColors.primaryTextColor,
          fontSize: Metrics.instance.headlineSmall,
          fontWeight: FontWeight.w500,
          letterSpacing: 0.15,
          overflow: TextOverflow.ellipsis,
        ),

        /// Used for large title
        titleLarge: TextStyle(
          color: AppColors.primaryTextColor,
          fontSize: Metrics.instance.titleLarge,
          fontWeight: FontWeight.w600,
          overflow: TextOverflow.ellipsis,
        ),

        /// Used for medium title
        titleMedium: TextStyle(
          color: AppColors.primaryTextColor,
          fontSize: Metrics.instance.titleMedium,
          fontWeight: FontWeight.w600,
          overflow: TextOverflow.ellipsis,
        ),

        /// Used for small title
        titleSmall: TextStyle(
          color: AppColors.primaryTextColor,
          fontSize: Metrics.instance.titleSmall,
          fontWeight: FontWeight.w500,
          overflow: TextOverflow.ellipsis,
        ),

        /// Used for large body
        bodyLarge: TextStyle(
          color: AppColors.primaryTextColor,
          fontSize: Metrics.instance.bodyLarge,
          fontWeight: FontWeight.w600,
          overflow: TextOverflow.ellipsis,
        ),

        /// Used for medium body
        bodyMedium: TextStyle(
          color: AppColors.primaryTextColor,
          fontSize: Metrics.instance.bodyMedium,
          fontWeight: FontWeight.w500,
          overflow: TextOverflow.ellipsis,
        ),

        /// Used for small body
        bodySmall: TextStyle(
          color: AppColors.primaryTextColor,
          fontSize: Metrics.instance.bodySmall,
          fontWeight: FontWeight.w400,
          overflow: TextOverflow.ellipsis,
        ),
      ),

      /// Elevated button theme pre-define
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          alignment: Alignment.center,
          minimumSize: Size(double.infinity, Metrics.instance.buttonHeight),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(Metrics.instance.buttonRadius),
          ),
          foregroundColor: AppColors.white,
          backgroundColor: AppColors.primary,
          textStyle: TextStyle(
            fontSize: Metrics.instance.bodyMedium,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
