import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

mixin ConnectivityMixin {
  final connectivity = Connectivity();

  /// To check this device's network connectivity
  Future<bool> isInConnection() async {
    var connectivityResult = await connectivity.checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      Fluttertoast.showToast(
        msg: ('message.unableToConnectToTheInternet'.tr),
        backgroundColor: Colors.grey,
        textColor: Colors.white,
      );
      return false;
    }
    return true;
  }
}
