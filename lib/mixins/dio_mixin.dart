import 'package:get_retrofit_boilerplate/singletons/singletons_index.dart';
import 'package:get_retrofit_boilerplate/themes/app_color.dart';
import 'package:get_retrofit_boilerplate/utils/utils_index.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

mixin DIOMixin {
  final connectivity = Connectivity();

  /// Get DIO which have already added interceptor
  Future<Dio?> getDio() async {
    final connectivityResult = await connectivity.checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      Fluttertoast.showToast(
        msg: ('message.unableToConnectToTheInternet'.tr),
        backgroundColor: AppColors.primary,
        textColor: Colors.white,
        toastLength: Toast.LENGTH_LONG,
        fontSize: Metrics.instance.bodyMedium,
      );
      return null;
    } else {
      final Dio dio = Dio();
      dio.interceptors.add(DIOInterceptor());
      return dio;
    }
  }
}
