import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:file_picker/file_picker.dart';

class ImagePickerUtil {
  void pickImage({
    required BuildContext context,
    required Function(XFile?) callback,
    bool isUploadFile = false,
  }) {
    final ImagePicker picker = ImagePicker();
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Wrap(
            children: <Widget>[
              isUploadFile
                  ? ListTile(
                      leading: const Icon(Icons.file_present),
                      title: const Text('File'),
                      onTap: () async {
                        Navigator.of(context).pop();
                        try {
                          final result = await FilePicker.platform.pickFiles();
                          if (result == null) return;
                          final file = result.files.first;

                          final XFile image = XFile(
                            file.path!,
                            name: file.name,
                            mimeType: file.extension,
                            lastModified: DateTime.now(),
                            length: file.size,
                          );
                          callback(image);
                        } catch (e) {
                          log(e.toString());
                        }
                      },
                    )
                  : const SizedBox(),
              ListTile(
                leading: const Icon(Icons.photo_library),
                title: const Text('PhotoLibrary'),
                onTap: () async {
                  Navigator.of(context).pop();
                  try {
                    final XFile? image = await picker.pickImage(
                        source: ImageSource.gallery, imageQuality: 100, maxHeight: 500, maxWidth: 500);
                    callback(image);
                  } catch (e) {
                    log(e.toString());
                  }
                },
              ),
              ListTile(
                leading: const Icon(Icons.photo_camera),
                title: const Text('Camera'),
                onTap: () async {
                  Navigator.of(context).pop();
                  try {
                    final XFile? image = await picker.pickImage(
                        source: ImageSource.camera, imageQuality: 100, maxHeight: 500, maxWidth: 500);
                    callback(image);
                  } catch (e) {
                    log(e.toString());
                  }
                },
              ),
            ],
          ),
        );
      },
    );
  }

  Future<List<XFile>?> pickMultipleImages() async {
    try {
      final ImagePicker picker = ImagePicker();
      final List<XFile> images = await picker.pickMultiImage(imageQuality: 100, maxHeight: 500, maxWidth: 500);
      return images;
    } catch (e) {
      log(e.toString());
      return null;
    }
  }
}
