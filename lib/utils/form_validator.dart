import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:validators/validators.dart';

/// Handle text field validation
class FormValidator {
  const FormValidator._();

  /// A method containing empty validation
  static String? emptyValidator({String? text, required String errorText}) {
    if (text == null || text.isEmpty) {
      return errorText;
    }
    return null;
  }

  /// A method containing phone number validation
  static String? phoneNumberValidator(
      {String? text, bool isRequire = true, String emptyErrorText = '', required String invalidPhoneNumberErrorText}) {
    if (isRequire) {
      if (text == null || text.isEmpty) {
        return emptyErrorText;
      } else {
        text = text.replaceAll(' ', '');
        if (text.length < 8) {
          return invalidPhoneNumberErrorText;
        }
      }
    } else {
      if (text != null && text.isNotEmpty) {
        text = text.replaceAll(' ', '');
        if (text.length < 8) {
          return invalidPhoneNumberErrorText;
        }
      }
    }
    return null;
  }

  /// A method containing validation logic for email input.
  static String? emailValidator({String? email, required String invalidErrorText, required String emptyErrorText}) {
    if (email == null || email.isEmpty) {
      return emptyErrorText;
    } else {
      if (!isEmail(email)) {
        return invalidErrorText;
      }
    }
    return null;
  }

  /// A method containing validation logic for confirm password input.
  static String? confirmPasswordValidator(
    String? confirmPw,
    String? inputPw, {
    String? emptyErrorText,
    required String passwordErrorText,
  }) {
    if (emptyErrorText == null) {
      if ((confirmPw == null || confirmPw.isEmpty) && (inputPw == null || inputPw.isEmpty)) {
        return null;
      } else if (confirmPw!.trim() == inputPw!.trim()) {
        return null;
      }
    } else {
      if (confirmPw == null || confirmPw.isEmpty) {
        return emptyErrorText;
      } else if (confirmPw.trim() == inputPw!.trim()) {
        return null;
      }
    }
    return passwordErrorText;
  }

  /// A method containing phone formatter
  static MaskTextInputFormatter phoneInputFormatter() {
    return MaskTextInputFormatter(
      mask: '### ### ####',
      filter: {
        '#': RegExp(r'[0-9]'),
        '&': RegExp(r'[1-9]'),
      },
    );
  }

  /// A method containing money formatter
  static MaskTextInputFormatter moneyInputFormatter() {
    return MaskTextInputFormatter(mask: '\$#,##0', filter: {
      '#': RegExp(r'[0-9]'),
      '&': RegExp(r'[1-9]'),
      '.': RegExp(r'\d+([\.]\d+)?'),
    });
  }
}
