import 'package:get_retrofit_boilerplate/themes/themes_index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:get/get.dart';

class DateTimeUtil {
  void showDate({
    required BuildContext context,
    required TextEditingController controller,
    required DateTime initialDateTime,
    required DateTime minimumDate,
    required DateTime maximumDate,
    required int minimumYear,
    required int maximumYear,
    Color color = AppColors.primary,
    Color btnTextColor = AppColors.primary,
    bool isInviteGuest = true,
    bool isEndDate = false,
  }) {
    if (Device.get().isAndroid) {
      _androidDatePicker(
        context,
        controller,
        initialDateTime,
        minimumDate,
        maximumDate,
        minimumYear,
        maximumYear,
        color: color,
        isEndDate: isEndDate,
      );
    } else {
      _iosDatePicker(
        context,
        controller,
        initialDateTime,
        minimumDate,
        maximumDate,
        minimumYear,
        maximumYear,
        btnTextColor: btnTextColor,
        isInviteGuest: isInviteGuest,
        isEndDate: isEndDate,
      );
    }
  }

  void _androidDatePicker(
    BuildContext context,
    TextEditingController controller,
    DateTime initialDateTime,
    DateTime minimumDate,
    DateTime maximumDate,
    int minimumYear,
    int maximumYear, {
    Color color = AppColors.primary,
    bool isEndDate = false,
  }) async {
    final newDate = await showDatePicker(
      context: context,
      initialDate: initialDateTime,
      firstDate: minimumDate,
      lastDate: maximumDate,
      builder: (context, child) {
        return Theme(
          data: Theme.of(context).copyWith(
            colorScheme: ColorScheme.light(primary: color),
          ),
          child: child!,
        );
      },
    );

    if (newDate == null) return;
    controller.text = newDate.toString().split(' ')[0];
  }

  void _iosDatePicker(
    BuildContext context,
    TextEditingController controller,
    DateTime initialDateTime,
    DateTime minimumDate,
    DateTime maximumDate,
    int minimumYear,
    int maximumYear, {
    Color btnTextColor = AppColors.primary,
    bool isInviteGuest = true,
    bool isEndDate = false,
  }) async {
    await showCupertinoModalPopup(
      context: context,
      builder: (context) {
        return CupertinoActionSheet(
          actions: [
            _buildCupertinoActionSheetItem(
              controller,
              initialDateTime,
              minimumDate,
              maximumDate,
              minimumYear,
              maximumYear,
            ),
          ],
          cancelButton: CupertinoActionSheetAction(
            child: Text(
              'button.done'.tr,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 18,
                color: btnTextColor,
              ),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        );
      },
    );
  }

  Widget _buildCupertinoActionSheetItem(
    TextEditingController controller,
    DateTime initialDateTime,
    DateTime minimumDate,
    DateTime maximumDate,
    int minimumYear,
    int maximumYear,
  ) {
    return SizedBox(
      height: 170,
      width: double.infinity,
      child: CupertinoDatePicker(
        initialDateTime: initialDateTime,
        minimumDate: minimumDate,
        maximumDate: maximumDate,
        minimumYear: minimumYear,
        maximumYear: maximumYear,
        mode: CupertinoDatePickerMode.date,
        onDateTimeChanged: (date) {
          controller.text = date.toString().split(' ')[0];
        },
      ),
    );
  }
}
