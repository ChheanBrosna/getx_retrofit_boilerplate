import 'dart:developer';
import 'package:get_retrofit_boilerplate/constants/constants_index.dart';
import 'package:get_retrofit_boilerplate/enums/enums_index.dart';
import 'package:get_retrofit_boilerplate/storage/storage_index.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Handle DIO Interceptor
class DIOInterceptor extends Interceptor {
  DIOInterceptor();

  @override
  Future<void> onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    final newOptions = await _setHeaders(options);
    log('===== API CALL =====');
    log('${newOptions.method}: ${newOptions.uri.toString()}');
    log('===== PAYLOADS =====');
    log(newOptions.data.toString());
    return super.onRequest(newOptions, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    log('===== RESPONSE ===== $response');
    super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    log('===== ERROR ===== $err : ${err.response}');
    if (err.type == DioErrorType.response) {
      switch (err.response?.statusCode) {
        case 401:
          err.error = ErrorMessageConstant.unauthorized;
          break;
        case 403:
          err.error = ErrorMessageConstant.forbidden;
          break;
        case 404:
          err.error = ErrorMessageConstant.notFound;
          break;
        case 500:
          err.error = ErrorMessageConstant.internalServerError;
          break;
      }
    }

    if (err.type == DioErrorType.cancel) err.error = ErrorMessageConstant.userCancelled;

    if (err.type == DioErrorType.sendTimeout || err.type == DioErrorType.receiveTimeout) {
      err.error = ErrorMessageConstant.serverTimeOut;
    }

    if (err.type == DioErrorType.connectTimeout) {
      err.error = ErrorMessageConstant.noConnection;
    }

    if (err.type == DioErrorType.other) {
      err.error = ErrorMessageConstant.unknown;
    }

    super.onError(err, handler);
  }

  /// Set headers
  Future<RequestOptions> _setHeaders(RequestOptions options) async {
    final authorization = options.headers['Authorization'];
    final accessToken = await _getToken();
    log('===== TOKEN ===== ${accessToken.toString()}');
    if (authorization == null) {
      options.headers['Authorization'] = accessToken;
    }
    return options;
  }

  /// Get token from local storage
  Future<String?> _getToken() async {
    final SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    final AppStorage appStorage = AppStorage(sharedPreferences: sharedPreferences);

    return 'Bearer ${appStorage.readString(key: StorageKey.token.name)}';
  }
}
