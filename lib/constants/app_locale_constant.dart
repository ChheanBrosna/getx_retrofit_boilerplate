import 'package:flutter/material.dart';

class AppLocalesConstant {
  static const Locale en = Locale('en', 'US');
  static const Locale km = Locale('km', 'KH');
}
