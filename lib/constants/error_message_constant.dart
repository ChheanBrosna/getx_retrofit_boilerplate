/// Handle shared_preferences key
class ErrorMessageConstant {
  static const String unknown = 'Unknown Error!';
  static const String noData = 'No Data!';
  static const String noConnection = 'Unable to establish connection!';
  static const String serverTimeOut = 'Server is not responding!';
  static const String unauthorized = 'You no longer has permission to use this feature!';
  static const String forbidden = 'Forbidden!';
  static const String notFound = 'Resource not found!';
  static const String internalServerError = 'Internal server error!';
  static const String userCancelled = 'User cancelled request!';
}
