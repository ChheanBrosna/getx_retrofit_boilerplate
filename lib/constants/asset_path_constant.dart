/// Handal assets path
class AssetPathConstant {
  static const String baseLauncherIcon = 'assets/app_icon/';
  static const String baseImage = 'assets/image/';
  static const String baseSvg = 'assets/svg/';

  // Launcher icon
  static const appIcon = '${baseLauncherIcon}app_icon.png';

  // Image
  static const cambodiaFlag = '${baseImage}cambodia-flag.png';
  static const chinessFlag = '${baseImage}chiness-flag.png';
  static const englishFlag = '${baseImage}english-flag.png';

  // Svg
  static const signout = '${baseSvg}signout.svg';
}
