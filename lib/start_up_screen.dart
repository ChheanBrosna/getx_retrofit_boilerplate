import 'package:get_retrofit_boilerplate/singletons/singletons_index.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class StartUpScreen extends StatelessWidget {
  const StartUpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Start Up'),
      ),
      body: Padding(
        padding: EdgeInsets.all(Metrics.instance.defaultPadding),
        child: Center(
          child: ElevatedButton(
            onPressed: () {},
            child: Text('button.done'.tr),
          ),
        ),
      ),
    );
  }
}
