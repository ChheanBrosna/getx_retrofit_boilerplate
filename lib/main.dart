import 'package:get/route_manager.dart';
import 'package:get_retrofit_boilerplate/constants/constants_index.dart';
import 'package:get_retrofit_boilerplate/routes/routes_index.dart';
import 'package:get_retrofit_boilerplate/themes/themes_index.dart';
import 'package:get_retrofit_boilerplate/translations/translations_index.dart';
import 'package:get_retrofit_boilerplate/utils/utils_index.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MaterialApp(home: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,

      title: AppConstant.appName,
      theme: AppTheme.define(),

      // locale config
      locale: AppLocalesConstant.en,
      fallbackLocale: AppLocalesConstant.en,
      translations: AppTranslations(),

      // route config
      initialRoute: RouteConstant.startUpScreen,
      getPages: AppRoute.pages,

      // dismiss keyboard when click on screen
      builder: (context, child) {
        return GestureDetector(
          onTap: () => KeyboardHidden.hideKeyboard(context),
          child: child,
        );
      },
    );
  }
}
