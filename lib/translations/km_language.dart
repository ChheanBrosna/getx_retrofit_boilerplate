class KmLanguage {
  static Map<String, String> get language {
    return {
      /// Message
      'message.unableToConnectToTheInternet': 'មិនអាចភ្ជាប់ទៅអ៊ីនធឺណិតបានទេ',

      /// Button
      'button.done': 'រួចរាល់',

      /// Screen
      'screen.camera': 'កាមេរ៉ា'
    };
  }
}
