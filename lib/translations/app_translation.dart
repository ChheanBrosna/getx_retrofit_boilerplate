import 'package:get/route_manager.dart';

import 'en_language.dart';
import 'km_language.dart';

class AppTranslations extends Translations {
  @override
  // implement keys
  Map<String, Map<String, String>> get keys {
    return {
      'en': EnLanguage.language,
      'km': KmLanguage.language,
    };
  }
}
