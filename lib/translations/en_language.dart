class EnLanguage {
  static Map<String, String> get language {
    return {
      /// Message
      'message.unableToConnectToTheInternet': 'Unable to connect to the internet',

      /// Button
      'button.done': 'Done',

      /// Screen
      'screen.camera': 'Camera'
    };
  }
}
