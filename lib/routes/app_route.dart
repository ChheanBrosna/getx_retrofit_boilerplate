import 'package:get/route_manager.dart';
import 'package:get_retrofit_boilerplate/constants/route_constant.dart';
import 'package:get_retrofit_boilerplate/start_up_screen.dart';

abstract class AppRoute {
  static final pages = [
    GetPage(
      name: RouteConstant.startUpScreen,
      page: () => const StartUpScreen(),
    )
  ];
}
